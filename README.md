# Installation (Linux/ Xubuntu)

    Open a terminal and type the following command:

    `sudo npm install -g browserify`

    Note that you must install it **globally**.

# Install packages

    Navigate to node_modules/ in your project directory, then type:

    `sudo npm install jquery`

# Creating Your First Browserify File

    Navigate to app/ in your project directory, create a js file with this:

    ```
    var $ = require('jquery');

    $(document).ready(function(){
        console.log($("#example").length);
        console.log('hello world');
    });
    ```

# Compile Browserify Files

    Navigate to app/ from the terminal and type:

    `browserify bundle.js -o ../dist/bundle.js`

# Using the Browserify Output

    Put this in your HTML head:

    `<script src="dist/bundle.js"></script>`

# References

1. http://omarfouad.com/
